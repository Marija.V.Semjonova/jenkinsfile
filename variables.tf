variable "aws_access_key_id" {}


variable "aws_secret_key_id" {}

variable "aws_region_id" {}
  # description = "the AWS region to deploy to"
  # default     = "eu-west-1"


variable "public_key_path" {
  description = "Public key path"
  default = "~/Desktop/RITI-/Mary-Jenkins-Key.pub"

}
variable "availability_zone" {
  description = "availability zone to create subnets"
  default = "eu-west-1a"
}
variable "user_data"{
 description = "User data file"
default = "~/Desktop/RITI-/userdata.tpl"
}
variable "user_data2"{
 description = "User data file 2"
default = "~/Desktop/RITI-/userdata2.tpl"
}