#!/bin/bash

    sed -i 's/Port 22/Port 443/g' /etc/ssh/sshd_config
    sed -i 's/#Port 443/Port 443/g' /etc/ssh/sshd_config
    service sshd restart

    yum update -y
    sudo yum install git -y
    sudo amazon-linux-extras install ansible2 -y
    git clone https://gitlab.com/Marija.V.Semjonova/jenkins.git
    sudo ansible-playbook jenkins/jenkins.yml -b
