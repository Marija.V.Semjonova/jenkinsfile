#!/bin/bash
sed -i 's/Port 22/Port 443/g' /etc/ssh/sshd_config
    sed -i 's/#Port 443/Port 443/g' /etc/ssh/sshd_config
    service sshd restart

sudo yum update -y
mkdir /home/ec2-user/jenkins
sudo chown -R ec2-user:ec2-user /home/ec2-user/jenkins
sudo yum install git java-1.8.0-openjdk-javadoc java-1.8.0-openjdk-devel -y
sudo mkdir -p /var/lib/jenkins/android-sdk/
sudo chown -R ec2-user:ec2-user /var/lib/jenkins
cd /var/lib/jenkins/android-sdk/
curl https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip -o android-sdk.zip
unzip android-sdk.zip -d .
cd tools
yes | sudo ./bin/sdkmanager --licenses
echo after license 
sleep 30
sudo ./bin/sdkmanager "tools"
echo after tools
sleep 10
cd ..
cd tools
sudo ./bin/sdkmanager "build-tools;26.0.2" "platforms;android-26" "platforms;android-28" "extras;android;m2repository" "extras;google;m2repository"
echo after build-tools
sleep 30
sudo ./bin/sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2"
echo after extras
sleep 10
sudo ./bin/sdkmanager --list
