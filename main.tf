provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_key_id}"
  region     = "${var.aws_region_id}"
}
resource "aws_key_pair" "Mary-Jenkins-Key-SSH" {
  key_name   = "Mary-Jenkins-Key-SSH"
  # public_key = var.ssh_key_public
  public_key = 
  tags = {
    Name = "key"
  }
}
resource "aws_vpc" "main" {
  cidr_block = ""
   enable_dns_support   = true
   enable_dns_hostnames = true

  tags = {
    Name = "Mary_vpc"
  }
}

resource "aws_subnet" "public_mary_subnet" {
  vpc_id     = aws_vpc.main.id
  cidr_block = ""
  availability_zone = "${var.availability_zone}"
  map_public_ip_on_launch = true
  tags = {
    Name = "Mary_public_subnet"
  }
}
resource "aws_route_table" "rt_mary" {
  vpc_id = "${aws_vpc.main.id}"

  route {
      cidr_block = ""
      gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags = {
	    Name = "route-table-mary"
 }
}
resource "aws_route_table_association" "public" {
  subnet_id      = "${aws_subnet.public_mary_subnet.id}"
  route_table_id = "${aws_route_table.rt_mary.id}"
}


resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_security_group" "mary_sec_group" {
  name        = "mary_sec_group"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [""]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [""]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [""]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [""]
  }
  # ingress {
  #   from_port   = 0
  #   to_port     = 0
  #   protocol    = "-1"
  #   cidr_blocks = ["${aws_security_group.mary_sec_group.id}"]
  # }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [""]
  }
}

 data "template_file" "user_data" {
  template = "${file("${path.module}/userdata.tpl")}"
}
data "template_file" "user_data2" {
  template = "${file("${path.module}/userdata2.tpl")}"
}
resource "aws_instance" "my_linux" {
  ami                    = "ami-099a8245f5daa82bf"
  instance_type          = "t2.micro"
  key_name               = "Mary-Jenkins-Key-SSH"
  subnet_id              = "${aws_subnet.public_mary_subnet.id}"
  vpc_security_group_ids = [aws_security_group.mary_sec_group.id]
  user_data = "${data.template_file.user_data.rendered}"
   tags = {
    Name = "MaryS - Master"
  }
}
  resource "aws_instance" "my_linux_2" {
  ami                    = "ami-099a8245f5daa82bf"
  instance_type          = "t2.medium"
  key_name               = "Mary-Jenkins-Key-SSH"
  subnet_id              = "${aws_subnet.public_mary_subnet.id}"
  vpc_security_group_ids = [aws_security_group.mary_sec_group.id]
  user_data = "${data.template_file.user_data2.rendered}"
   tags = {
    Name = "MaryS - Slave"
  }
  }







#    user_data              = <<-EOF
#     #!/bin/bash

#     sed -i 's/Port 22/Port 443/g' /etc/ssh/sshd_config
#     sed -i 's/#Port 443/Port 443/g' /etc/ssh/sshd_config
#     service sshd restart

#     yum update -y
#     sudo yum install git -y
#     sudo amazon-linux-extras install ansible2 -y
#     git clone https://gitlab.com/Marija.V.Semjonova/jenkins.git
#     sudo ansible-playbook jenkins/jenkins.yml -b
#     sudo mkdir /home/ec2-user/jenkins
# EOF

